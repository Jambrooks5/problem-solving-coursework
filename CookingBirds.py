import math
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage.morphology as morph

#Parameters in SI
Time = 150 #Length of time to simulate
startTemp = 41 #degress Celsius
iterations = 300 #Number of time steps to use
resolution = 50 #Number of pixels across bird

#SIdiam = 0.15 #chicken
#SIdiam = 0.13 #duck
SIdiam = 0.08 #godwit

alpha = 1.5*10**(-7) #m^2 s^-1
SIdx = SIdiam/resolution #x step size
SIdt = Time/iterations #time step size
g = (alpha * SIdt) / (SIdx ** 2) #gamma
SIc = 2720 #specific heat cap. [J kg^-1 K^-1]
rhoC = 1000*SIc #density * heat cap
SIdV = SIdx**3 #'volume' of each square
F = 1*10**5 #Light flux coming from mirror [W m^-2]
vapourTemp = 800 #temperature of vapourisation [K]
SIdE = F * SIdt * (SIdx)**2 #Energy given per dx^2 per time step [J]


#change of temp of each cell per time step from incident radiation [K]
dT = SIdE / (rhoC * SIdV)

#Calculating mass of bird
mass = ((4/3)*np.pi*(SIdiam/2)**3) * 1000
print("Mass of bird(kg): ", mass)
diam = resolution
dx=1

#print(SIdiam, alpha, SIdx, SIdt, g, SIdE, rhoC, dT)
print("Cooking time: ", iterations*SIdt, "s")

#Create empty array
u = np.empty((iterations, diam+1, diam+1))
u.fill(0)

#Creat circle to represent bird
xx, yy = np.mgrid[:diam+1, :diam+1]
circle = (xx - diam/2) ** 2 + (yy - diam/2) ** 2
u[:,:,:] = (273+startTemp)*(circle < ((diam/2)**2))


####Setting the pixels exposed to death ray
def findSurface(u_k):
    birdMask_k = u_k > 0
    
    ###For heating whole surface
    inner = morph.binary_erosion(u_k)#.astype(u[0].dtype)
    incidentMask_k = birdMask_k & ~inner
   
    ###For heating from left side
    #incidentMask_k = np.zeros_like(birdMask_k, dtype=int)
    #incidentMask_k[:, 1:] = birdMask_k[:, 1:] & ~birdMask_k[:, :-1]

    ###Temp for demonstration grid
    #incidentMask_k[1,1] = 1 

    return birdMask_k, incidentMask_k 


birdMask, incidentMask = np.zeros_like(u), np.zeros_like(u, dtype=bool)
#Converting circle mask from boolean to binary to use in calculate(u) to prevent contribution from background pixels
birdMask[0], incidentMask[0] = findSurface(u[0])
birdMask[0] = 1*birdMask[0]


def calculate(u):
    #iterate through time steps
    for k in range(0, iterations-1, 1):
        ###Use for incident energy flux
        u[k,incidentMask[k]] = u[k,incidentMask[k]] + dT
        
        ###Holds exposed pixels at fixed temperature - modelling oven
        #u[k, incidentMask[k]] = 473
        
        for i in range(1, diam, dx): #iterate through y (yes it's the other way around in the report)
            for j in range(1, diam, dx): #iterate through x
                if (birdMask[k][i,j] == 1): 
                    #Count number of adjacent cells that are part of the circle
                    adjacent = birdMask[k][i+1][j] + birdMask[k][i-1][j] + birdMask[k][i][j-1] + birdMask[k][i][j+1]
                    u[k + 1, i, j] = g * (u[k][i+1][j]*birdMask[k][i+1][j] + u[k][i-1][j]*birdMask[k][i-1][j] + u[k][i][j+1]*birdMask[k][i][j+1] + u[k][i][j-1]*birdMask[k][i][j-1] - adjacent*u[k][i][j]*birdMask[k][i][j]) + u[k][i][j]*birdMask[k][i][j]
      
        vapourised = u[k+1] > vapourTemp
        u[k+1:,vapourised] = 0
        birdMask[k+1], incidentMask[k+1] = findSurface(u[k+1])
        birdMask[k+1] = 1*birdMask[k+1]
       
    return u


# Do the calculation here
u = calculate(u)
print("Lowest temp(K): ", np.amin(u[iterations-1]>0))
print("Highest temp(K): ", np.amax(u[iterations-1, :, :]))

#Plots every time step
'''
for i in range(0,iterations):
    fig = plt.figure(dpi=300)
    plt.imshow(u[i], cmap='hot', interpolation='nearest',vmin=273+startTemp-15, vmax=347)
    plt.colorbar()
    plt.show()
'''
#Plots only final time step
fig = plt.figure(dpi=300)
plt.axis('off')
plt.imshow(u[iterations-1], cmap='hot', interpolation='nearest',vmin=273+startTemp-15, vmax=347)
plt.colorbar(label="Temperature (K)")
name = f"{Time}sec-{iterations}its-{F}Wm-2.png"
plt.savefig(name)

