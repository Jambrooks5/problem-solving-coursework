import matplotlib.pyplot as plt
import numpy as np
import scienceplots

plt.style.use(['science','no-latex'])

m = 0.3
rhoa = 0.5*1.293
rhob = 1000
cd = 0.5
g = 9.81

alpha = np.sqrt(g)
gamma = np.sqrt(rhoa*cd*((3*np.sqrt(2*np.pi))/(16*rhob*np.sqrt(m)))**(2/3))

heights = np.arange(0, 6000)

dragTimes = (1/(gamma*alpha))*np.arccosh(np.exp(heights*gamma**2))
suvatTimes = np.sqrt(2*heights/g)
 
fig = plt.figure(dpi=300)
plt.xlabel("Initial height (m)")
plt.ylabel("Fall time (s)")
fig.patch.set_facecolor('white') 
         
plt.plot(heights, dragTimes, label="Our model drag")
plt.plot(heights, suvatTimes, label="SUVAT")
plt.legend()
plt.show()
